const BASE_URL = 'http://localhost:3000';

function extractJsonFromBody(response) {
    return response.json();
}

export function fetchComputers() {
    //make request to server
    return fetch(`${BASE_URL}/computers`)
        .then(extractJsonFromBody);
}

export function fetchComputerById(id) {
    //make request to server
    return fetch(`${BASE_URL}/computers/${id}`)
        .then(extractJsonFromBody);
}

export function createComputer(computer) {
    // TODO implement post for comuter
}
