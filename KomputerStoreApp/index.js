import { fetchComputers } from './api.js'

class App {
    constructor() {
        //HTML elements
        this.elStatus = document.getElementById('app-status');
        this.elComputerSelect = document.getElementById('computers');
        this.elSelectedComputer = document.getElementById('selected-computer');
        this.elComputerInformation = document.getElementById('product-info');

        //Properties
        this.computers = [];
        this.selectedComputer = null;
    }

    onComputerChange() {
        if (parseInt(this.elComputerSelect.value) === -1) {
            //reset computer 
            this.elComputerInformation.innerHTML = ' ';
            return;
        }

        this.selectedComputer = this.computers.find(computer => {
            return computer.id == this.elComputerSelect.value;
        })

        this.setComputer(this.selectedComputer);
    }

    async init() {
        //Event listener 
        this.elComputerSelect.addEventListener('change', this.onComputerChange.bind(this));

        this.elStatus.innerText = ' Loading computers...';
        this.elComputerSelect.disabled = true;

        try {
            this.computers = await fetchComputers();
            const elComputer = document.createElement('option');
            elComputer.innerText = '--Select a computer--';
            elComputer.value = -1;
            this.elComputerSelect.appendChild(elComputer);

            this.computers.forEach(computer => {
                const elComputer = document.createElement('option');
                elComputer.innerText = computer.name;
                elComputer.value = computer.id;
                this.elComputerSelect.appendChild(elComputer);
            })

            this.elStatus.innerText = '';

        } catch (e) {
            this.elStatus.innerText = 'Error! ' + e.messages;
        }
        finally {
            this.elComputerSelect.disabled = false;
        }
        this.render();

        console.log(this.computers);
    }

    setComputer(computer) {
        this.computer = computer
        this.render()
    }

    render() {

        this.elComputerInformation.innerHTML = ''

        // Create a new container for the computer to be rendered in.
        const elComputerContainer = document.createElement('div')
        const elComputerImageContainer = document.createElement('div')

        const image = new Image()
        image.width = 320
        image.setAttribute('id', 'product-image')
        image.onload = () => {
            elComputerImageContainer.appendChild(image)
        }

        image.src = this.computer.image
        elComputerContainer.appendChild(elComputerImageContainer)

        const elComputerName = document.createElement('h1')
        elComputerName.innerText = this.computer.name
        elComputerContainer.appendChild(elComputerName)

        const elComputerPrice = document.createElement('h4')
        elComputerPrice.setAttribute('id', 'product-price')

        elComputerPrice.innerText = `NOK ${this.computer.price}`
        elComputerContainer.appendChild(elComputerPrice)

        const elComputerDescription = document.createElement('p')
        elComputerDescription.innerText = this.computer.description
        elComputerContainer.appendChild(elComputerDescription)
        elComputerContainer.setAttribute('id', 'product-description');

        const elComputerSpecs = document.createElement('span')
        elComputerSpecs.innerText = `Specs: ${this.computer.specs}`
        elComputerContainer.appendChild(elComputerSpecs)

        this.elComputerInformation.appendChild(elComputerContainer)

    }
}

//initalise the app 
new App().init();


//Global Variables
let currentBalance = 16500;
let loanActive = false;
let currentLoan = 0, currentPaycheck = 0;
let laptop = 12000;

//DOM elements
const totalBalance = document.createElement('td');
document.getElementById('active-balance').appendChild(totalBalance);

const activeLoanTitle = document.createElement('td');
activeLoanTitle.className = 'body-font';
document.getElementById("active-debt").appendChild(activeLoanTitle);
const activeLoanAmount = document.createElement('td');
document.getElementById("active-debt").appendChild(activeLoanAmount);

const paySlip = document.getElementById("workCount");
const btnPay = document.getElementById('payButton');


//Calculation
function maxLoan(balance) {
    return balance * 2;
}

function calculateTaxes(x) {
    return x * 0.1;
}

const maximumLoan = maxLoan(currentBalance);

//Update balance and loan with currency 
const updateTotalBalanceAndCurrentLoan = () => {
    const nokTotalBalance = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(currentBalance + currentLoan);
    totalBalance.innerHTML = nokTotalBalance;

    if (loanActive) {
        const nokLoan = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(currentLoan);
        activeLoanAmount.innerHTML = nokLoan;
        activeLoanTitle.innerHTML = 'Active Loan: ';
    }
}

//Display balance and active loan
updateTotalBalanceAndCurrentLoan();

//Aply for loan 
const btnApplyForLoan = document.getElementById('btn-request-loan');
btnApplyForLoan.addEventListener('pointerdown', e => {

    if (loanActive) {
        alert('Please pay back your current loan before requesting a new one')
        return
    }

    currentLoan = parseInt(prompt('How much money do you need?'));

    if (isNaN(currentLoan)) {
        alert("Please enter a number");
    }
    else if (currentLoan < 1 || currentLoan > maximumLoan) {
        alert("Not sufficient balance for a loan");
    }

    else {
        addPayBtn();
        loanActive = true;
        activeLoanTitle.style.visibility = "visible";
        activeLoanAmount.style.visibility = "visible";

        updateTotalBalanceAndCurrentLoan();
    }


});

const addPayBtn = () => (loanActive) ? btnPay.style.visibility = "hidden" : btnPay.style.visibility = "visible"

function work() {
    currentPaycheck += 100;
    paySlip.innerHTML = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(currentPaycheck);
}

//WORK BUTTON ---- increase by 100
const increaseButton = document.getElementById("increaseButton");
increaseButton.addEventListener('pointerdown', e => {
    work();
    if (currentPaycheck === 2000) {
        alert('You should take a vacation! ');
    }
})

//----------------------------BANK BUTTON
let taxesToPay = 0;
const btnTransferSalary = document.getElementById('decreaseButton');
btnTransferSalary.addEventListener('pointerdown', e => {

    if (loanActive) {
        taxesToPay = calculateTaxes(currentPaycheck);
        currentLoan -= taxesToPay;
        currentBalance += currentPaycheck - taxesToPay;
        updateTotalBalanceAndCurrentLoan();
    }

    currentBalance += currentPaycheck;
    updateTotalBalanceAndCurrentLoan();

    currentPaycheck = 0;
    paySlip.innerHTML = 0;

});

//PAY BUTTON - Reset Loan 
const btnPayDebt = document.getElementById('payButton');
btnPayDebt.addEventListener('pointerdown', e => {

    //laptop vlaue set to 12000
    if (currentPaycheck >= currentLoan) {
        currentBalance += currentPaycheck - currentLoan;
        currentLoan = 0;
        updateTotalBalanceAndCurrentLoan();
        document.getElementById("payButton").style.visibility = "hidden";
        activeLoanTitle.style.visibility = "hidden";
        activeLoanAmount.style.visibility = "hidden";
        loanActive = false;
    }

    currentLoan -= currentPaycheck;
    console.log('else');


    currentPaycheck = 0;
    paySlip.innerHTML = 0;
    updateTotalBalanceAndCurrentLoan();

});



const btnPurchase = document.getElementById('btn-buy-now');
btnPurchase.addEventListener('pointerdown', e => {

    let newComputer = 'Congratulations on your new awsome laptop!';

    let laptopPriceTag = document.getElementsByTagName('h4')[0].innerHTML;
    let laptopPrice = parseInt(laptopPriceTag.match(/-*[0-9]+/));

    if ((currentBalance + currentLoan) > laptopPrice) {
        currentBalance -= laptopPrice;
        alert(newComputer);
        updateTotalBalanceAndCurrentLoan();
    }
    else {
        alert("You don't have enough money to buy this computer");
    }


});







//register which button has been clicked and the name of the button
document.body.addEventListener('pointerdown', event => {
    if (event.target.nodeName === "BUTTON") {
        console.log('Clicked', event.target.textContent);
    }
})


